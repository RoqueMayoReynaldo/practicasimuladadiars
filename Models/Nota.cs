﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroFinal.Models
{
    public class Nota
    {
        public int id { get; set; }
        public String titulo { get; set; }
        public DateTime fechaModificacion { get; set; }
        public String contenido { get; set; }

        public List<NotaEtiqueta> notaEtiquetas { get; set; }

    }
}
