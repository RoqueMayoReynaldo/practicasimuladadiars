﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroFinal.Models
{
    public class NotaEtiqueta
    {
        public int id { get; set; }
        public int idNota { get; set; }
        public int idEtiqueta { get; set; }

        public Nota nota { get; set; }

        public Etiqueta etiqueta { get; set; }


    }
}
