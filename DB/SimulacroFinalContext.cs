﻿using Microsoft.EntityFrameworkCore;
using SimulacroFinal.DB.Mapping;
using SimulacroFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroFinal.DB
{
   

    public class SimulacroFinalContext : DbContext
    {
        public DbSet<Nota> Notas { get; set; }
        public DbSet<Etiqueta> Etiquetas { get; set; }
        public DbSet<NotaEtiqueta> NotaEtiquetas { get; set; }

        public SimulacroFinalContext(DbContextOptions<SimulacroFinalContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new NotaMap());
            modelBuilder.ApplyConfiguration(new EtiquetaMap());
            modelBuilder.ApplyConfiguration(new NotaEtiquetaMap());
        }

    }

}
