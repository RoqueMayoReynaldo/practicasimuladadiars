﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimulacroFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroFinal.DB.Mapping
{
    

    public class EtiquetaMap : IEntityTypeConfiguration<Etiqueta>

    {
        public void Configure(EntityTypeBuilder<Etiqueta> builder)
        {


            builder.ToTable("Etiqueta", "dbo");
            builder.HasKey(Etiqueta => Etiqueta.id);


        }
    }
}
