﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimulacroFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroFinal.DB.Mapping
{




    public class NotaEtiquetaMap : IEntityTypeConfiguration<NotaEtiqueta>

    {
        public void Configure(EntityTypeBuilder<NotaEtiqueta> builder)
        {


            builder.ToTable("NotaEtiqueta", "dbo");
            builder.HasKey(NotaEtiqueta => NotaEtiqueta.id);
            builder.HasOne(NotaEtiqueta => NotaEtiqueta.nota).WithMany(Nota=>Nota.notaEtiquetas).HasForeignKey(NotaEtiqueta => NotaEtiqueta.idNota);

            builder.HasOne(NotaEtiqueta => NotaEtiqueta.etiqueta).WithMany().HasForeignKey(NotaEtiqueta => NotaEtiqueta.idEtiqueta);

        }
    }
}
