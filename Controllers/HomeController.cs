﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SimulacroFinal.DB;
using SimulacroFinal.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SimulacroFinal.Controllers
{

    public class NE {

        public List<Etiqueta> etiquetas { get; set; }
        public List<Nota> notas { get; set; }
    }

    public class HomeController : Controller
    {
        private SimulacroFinalContext context;

        public HomeController(SimulacroFinalContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }



        public ViewResult Main()
        {
            NE notaEtiqueta = new NE
            {

                etiquetas = context.Etiquetas.ToList(),
                notas = context.Notas.ToList()
            };



            return View(notaEtiqueta);
        }

        public ViewResult formAdd()
        {




            return View();
        }

        [HttpPost]
        public ViewResult crearNota(Nota nota, String tags)
        {
            nota.fechaModificacion = DateTime.Now;
            context.Notas.Add(nota);
            context.SaveChanges();

            //convertir los tags separados por coma en una lista de string
            List<String> tgs = tags.Split(',').ToList();

            //obtenemos lista de eitquetas
            List<Etiqueta> etiquetas = context.Etiquetas.ToList();

            //obtenemos lista de elementos que no estan las etiquetas

            List<String> listaNueva = tgs.Except(etiquetas.Select(o => o.nombre)).ToList();


            //guardamos todos los que no esten en la lista
            foreach (String n in listaNueva)
            {
                Etiqueta et = new Etiqueta
                {
                    nombre = n
                };
                context.Etiquetas.Add(et);
                context.SaveChanges();

            }

            //agregamos la union de los tags

            List<Etiqueta> etiquets = context.Etiquetas.ToList();

            foreach (String t in tgs)
            {
                foreach (Etiqueta e in etiquets)
                {
                    if (t == e.nombre)
                    {
                        NotaEtiqueta nte = new NotaEtiqueta
                        {
                            idEtiqueta = e.id,
                            idNota = nota.id


                        };

                        context.NotaEtiquetas.Add(nte);
                        context.SaveChanges();

                    }

                }

            }

            NE notaEtiqueta = new NE
            {

                etiquetas = context.Etiquetas.ToList(),
                notas = context.Notas.ToList()
            };


            return View("Main", notaEtiqueta);
        }


        [HttpPost]
        public ViewResult listaByTag(int id)
        {


            List<NotaEtiqueta> notaEtiquetas = context.NotaEtiquetas.Include("nota").Where(o => o.idEtiqueta == id).ToList();



            return View(notaEtiquetas);

        }
        [HttpPost]
        public ViewResult listaTodos()
        {


            List<Nota> notas = context.Notas.ToList();



            return View(notas);

        }



        [HttpPost]
        public ViewResult search(string buscar)
        {
            if (string.IsNullOrEmpty(buscar))
            {
                List<Nota> n = context.Notas.ToList();
                return View("listaTodos", n);
            }

            //filtramos por titulo,contenido y etiquetas
            List<Nota> nota = context.Notas.Include("notaEtiquetas.etiqueta").Where(o => o.titulo.Contains(buscar) || o.contenido.Contains(buscar) || o.notaEtiquetas.Select(p => p.etiqueta.nombre).Contains(buscar)).ToList();



            return View("listaTodos", nota);

        }


        [HttpPost]
        public ViewResult getNota(int id)
        {
            Nota nota = context.Notas.Include("notaEtiquetas.etiqueta").FirstOrDefault(o => o.id == id);
            return View(nota);

        }


        [HttpGet]
        public ViewResult formEditar(int id)
        {
            Nota nota = context.Notas.Include("notaEtiquetas.etiqueta").FirstOrDefault(o => o.id == id);


            return View(nota);

        }


        [HttpPost]
        public ViewResult guardarEditado(Nota nota, String tags)
        {
            List<NotaEtiqueta> neMe = context.NotaEtiquetas.Where(o => o.idNota == nota.id).ToList();

            List<NotaEtiqueta> neAjenas = context.NotaEtiquetas.Where(o => o.idNota != nota.id).ToList();

            List<Etiqueta> etiquetasBorrar = new List<Etiqueta>();

            bool delet = true;
            foreach (NotaEtiqueta n in neMe)
            {
                foreach (NotaEtiqueta ne in neAjenas)
                {
                    if (ne.idEtiqueta == n.idEtiqueta)
                    {
                        delet = false;
                    }

                }

                if (delet)
                {


                    Etiqueta etiqueta = context.Etiquetas.FirstOrDefault(o => o.id == n.idEtiqueta);
                    etiquetasBorrar.Add(etiqueta);

                }

                delet = true; ;
            }

            context.NotaEtiquetas.RemoveRange(neMe);
            context.SaveChanges();

            if (etiquetasBorrar != null)
            {
                context.Etiquetas.RemoveRange(etiquetasBorrar);
                context.SaveChanges();
            }



            //////
            ///

            Nota note = context.Notas.FirstOrDefault(o => o.id == nota.id);

            note.fechaModificacion = DateTime.Now;
            note.titulo = nota.titulo;
            note.contenido = nota.contenido;
            context.SaveChanges();

            //convertir los tags separados por coma en una lista de string
            List<String> tgs = tags.Split(',').ToList();

            //obtenemos lista de eitquetas
            List<Etiqueta> etiquetas = context.Etiquetas.ToList();

            //obtenemos lista de elementos que no estan las etiquetas

            List<String> listaNueva = tgs.Except(etiquetas.Select(o => o.nombre)).ToList();


            //guardamos todos los que no esten en la lista
            foreach (String n in listaNueva)
            {
                Etiqueta et = new Etiqueta
                {
                    nombre = n
                };
                context.Etiquetas.Add(et);
                context.SaveChanges();

            }

            //agregamos la union de los tags

            List<Etiqueta> etiquets = context.Etiquetas.ToList();

            foreach (String t in tgs)
            {
                foreach (Etiqueta e in etiquets)
                {
                    if (t == e.nombre)
                    {
                        NotaEtiqueta nte = new NotaEtiqueta
                        {
                            idEtiqueta = e.id,
                            idNota = note.id


                        };

                        context.NotaEtiquetas.Add(nte);
                        context.SaveChanges();

                    }

                }

            }

            NE notaEtiqueta = new NE
            {

                etiquetas = context.Etiquetas.ToList(),
                notas = context.Notas.ToList()
            };











            return View("Main", notaEtiqueta);

        }


        [HttpGet]
        public ViewResult eliminarNota(int id)
        {


            Nota nota = context.Notas.Include("notaEtiquetas.nota").FirstOrDefault(o=>o.id==id);

            List<NotaEtiqueta> neMe = context.NotaEtiquetas.Where(o => o.idNota == nota.id).ToList();

            List<NotaEtiqueta> neAjenas = context.NotaEtiquetas.Where(o => o.idNota != nota.id).ToList();

            List<Etiqueta> etiquetasBorrar = new List<Etiqueta>();

            bool delet = true;
            foreach (NotaEtiqueta n in neMe)
            {
                foreach (NotaEtiqueta ne in neAjenas)
                {
                    if (ne.idEtiqueta == n.idEtiqueta)
                    {
                        delet = false;
                    }

                }

                if (delet)
                {


                    Etiqueta etiqueta = context.Etiquetas.FirstOrDefault(o => o.id == n.idEtiqueta);
                    etiquetasBorrar.Add(etiqueta);

                }

                delet = true; ;
            }

            context.NotaEtiquetas.RemoveRange(neMe);
            context.SaveChanges();

            if (etiquetasBorrar != null)
            {
                context.Etiquetas.RemoveRange(etiquetasBorrar);
                context.SaveChanges();
            }


            context.Notas.Remove(nota);
            context.SaveChanges();





            NE notaEtiqueta = new NE
            {

                etiquetas = context.Etiquetas.ToList(),
                notas = context.Notas.ToList()

            };






            return View("Main", notaEtiqueta);
        }


    }
}
